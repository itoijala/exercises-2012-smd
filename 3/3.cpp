#include <iostream>
#include <cmath>
#include <cstdio>

#include "TCanvas.h"
#include "TGraph.h"
#include "TAxis.h"
using namespace std;

void ex7()
{
	/*
	 * f(x) = (x³ + 1/3) - (x³ - 1/3) = 2/3
	 *
	 * float:  x <= 8:     very good
	 *         x <= 60:    max 1% difference
	 *         x >= 110:   f(x) = -1/3
	 * double: x <= 100:   very good
	 *         x <= 5e4:   max 1% difference
	 *         x >= 1.1e5: f(x) = -1/3
	 */
	TGraph *gf = new TGraph();
	TGraph *gd = new TGraph();
	for (double x = 0; x <= 10000000; x += 1)
	{
		volatile float xf  = x;
		volatile float x3  = xf;
		               x3 *= xf;
		               x3 *= xf;
		volatile float ff  = x3;
		               ff += 1.0f / 3.0f;
		               ff -= x3;
		               ff += 1.0f / 3.0f;
		gf->SetPoint(gf->GetN(), xf, ff - 2.0 / 3.0);

		double fd = x * x * x + 1.0 / 3.0 - x * x * x + 1.0 / 3.0;
		gd->SetPoint(gd->GetN(), x, fd - 2.0 / 3.0);
	}

	TCanvas *canvas = new TCanvas("canvas7a", "");
	canvas->Divide(1, 2);
	canvas->cd(1);
	gf->Draw("AL");
	gPad->SetLogx();
	canvas->cd(2);
	gd->Draw("AL");
	gPad->SetLogx();
	canvas->Update();
#if !defined(__CINT__)
	canvas->SaveAs("7a.pdf");
#endif

	/*
	 * f(x) = ((3 + x³/3) - (3 - x³/3)) / x³ = 2/3
	 *
	 * float:  x <= 0.007: f(x) = -1/3
	 *         x >= 0.007: not so good
	 *         x >= 1:     very good
	 * double: x -> 0:     f(x) -> -1/3
	 *         x >= 1e-4:  very good
	 */
	gf = new TGraph();
	gd = new TGraph();
	for (double x = 0.000000001; x <= 1.1; x += 0.0001)
	{
		volatile float xf  = x;
		volatile float x3  = xf;
		               x3 *= xf;
		               x3 *= xf;
		volatile float xt  = x3 / 3.0f;
		volatile float ff  = 3.0f;
		               ff += xt;
		               ff -= 3.0f;
		               ff += xt;
		               ff /= x3;
		gf->SetPoint(gf->GetN(), xf, ff - 2.0 / 3.0);

		double fd = (3.0 + x * x * x / 3.0 - 3.0 + x * x * x / 3.0) / (x * x * x);
		gd->SetPoint(gd->GetN(), x, fd - 2.0 / 3.0);
	}

	canvas = new TCanvas("canvas7b", "");
	canvas->Divide(1, 2);
	canvas->cd(1);
	gf->Draw("AL");
	gPad->SetLogx();
	canvas->cd(2);
	gd->Draw("AL");
	gPad->SetLogx();
	canvas->Update();
#if !defined(__CINT__)
	canvas->SaveAs("7b.pdf");
#endif
}

void ex8()
{
	/*
	 * a) 1          = +1·2^0 (float & double)
	 * b) 0.1        ≈ +10011001100110011001101·2^1111011 (float)
	 *               ≈ +1001100110011001100110011001100110011001100110011010·2^1111111011 (double)
	 * c) 0.5        = +1·2^11111111 (float & double)
	 * d) 1/3        ≈ +1010101010101010101011·2^1111101 (float)
	 *    1/3        ≈ +101010101010101010101010101010101010101010101010101·2^1111111101 (double)
	 * e) π          ≈ +10010010000111111011011·2^10000000 (float) (= 3.14159265358979)
	 *    π          ≈ +1001001000011111101101010100010001000010110100011000·2^10000000000 (double) (= 3.141592653589793238462)
	 * f) 1000001    = +11110100001001000001·2^10100 (float & double)
	 * g) 1000000001 ≈ +11011100110101100101000·2^10011100 (float) (= 1000000000)
	 *    1000000001 = +111011100110101100101000000001·2^11110 (double)
	 * h) 1e40       ≈ 01111111100000000000000000000000 (float) (= inf)
	 *    1e40       ≈ +1101011000110010100111110001110000110101110010100101·2^10010000011 (double) (= 10000000000000000303786028427003666890752)
	 */
}

void ex9()
{
 	/*
 	 * b, c, d)
 	 */
	double a = 1.0 / 137.0;
	double a2 = a * a;
	double PI = 4.0 * atan(1.0);
	double E_e = 50.0;
	double m_e = 0.511;
	double s = 4 * E_e * E_e;
	double g = E_e / m_e;
	double g2 = g * g;
	double b = sqrt(1.0 - 1.0 / g2);
	double b2 = 1.0 - 1.0 / g2;
	double b4 = b2 * b2;

	int n = 1000;

 	double *t = new double[n];
 	double *c = new double[n];
	double *dc = new double[n];
	double *K = new double[n];
	t[0] = 0;

	for (int i = 0; i < n; i++)
	{
		t[i] = i * PI / n;
		double st = sin(t[i]);
		double st2 = st * st;
		double st4 = st2 * st2;
		double ct = cos(t[i]);
		double denom = 1 / g2 + b2 * st2;
		double denom2 = denom * denom;
		c[i] = a2 / s * ((2 + st2) / denom - 2 * st4 / denom2);

		denom = b2 * g2 * st2 + 1;
		double denom3 = denom * denom * denom;
		dc[i] = -2 * a2 * g2 / s * st * ct * (2 * b2 * g2 + g2 * (2 * b4 * g2 - b*2 + 4) * st2 - 1) / denom3;
		K[i] = abs(t[i] * dc[i] / c[i]);
	}

 	TCanvas *canvas = new TCanvas("canvas9d", "");
	TGraph *graph1 = new TGraph(n, t, c);
	graph1->SetTitle("Cross Section");
	TGraph *graph2 = new TGraph(n, t, K);
	graph2->SetTitle("Condition");
	canvas->Divide(2, 1);
	canvas->cd(1);
	graph1->Draw("AL");
	canvas->cd(2);
	graph2->Draw("AL");
	canvas->Update();

#if !defined(__CINT__)
	canvas->SaveAs("9.pdf");
#endif
}

void ex10()
{
	/*
	 * a)
	 * possibilities: (3,6),(4,5),(5,4),(6,3)
	 * => p = 100% * 4 / 36 = 11,11111%
	 *
	 * b)
	 * more possibilities: (5,5),(4,6),(6,4)
	 * => p = 100% * 7 / 36 = 19,44444%
	 *
	 * c)
	 * two possibilities (4,5),(5,4), with a 1/36 chance each.
	 * => p = 100% * 2 / 36 = 5,55555%
	 *
	 * d)
	 * Just one possibility (4,5)
	 * => p = 100$ * 1 / 36 = 2,7777%
	 *
	 * From now on the blue die is the only random variable.
	 * The red die is always = 4.
	 *
	 * e)
	 * One possibility (5)
	 * => p = 100% * 1 / 6 = 16,66666%
	 *
	 * f)
	 * Two possibilities (5),(6)
	 * => p = 100% * 2 / 6 = 33,3333333%
	 *
	 * g)
	 * One possibility (5)
	 * => p = 100% * 1 / 6 = 16,66666%
	 *
	 * h)
	 * One possibility (5)
	 * => p = 100% * 1 / 6 = 16,66666%
	 */
}

#if !defined(__CINT__)

int main()
{
	ex7();
	ex9();
	return 0;
}

#endif
