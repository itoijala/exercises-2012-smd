/*
 *  Statistische Methoden der Datenanalyse
 *  
 *  Übungsgruppe 1 / Wacker
 *
 *              Christian-Roman Gerhorst
 *              Matrikelnummer: 140173
 *
 *               Blatt 1 - Aufgabe 2
 *
*/

#include <iostream>
#include <fstream>

#include "TRandom3.h"
#include "TCanvas.h"
#include "TH1D.h"
#include "TPaveStats.h"
#include "TLegend.h"

using namespace std;

void writeToFile(){

    ofstream outp;
    outp.open("Datei1.txt");

    for (int i = 1; i <= 100; i++)
    {
        outp << i << endl;
    }

    outp.close();

    outp.open("Datei2.txt");
    TRandom3 random(0);

    for (int i = 1; i <= 100; i++)
    {
        outp << i << "   " << random.Integer(100) << endl;
    }

    outp.close();
}

void fileToHistogram(){
    int a1dim[100];
    int a2dim[100][2];

    ifstream inp;
    inp.open("Datei1.txt");

    for (int i = 0; i < 100; i++)
    {
        inp >> a1dim[i];
    }
    inp.close();

    inp.open("Datei2.txt");
    for (int i = 0; i < 100; i++)
    {
        inp >> a2dim[i][0] >> a2dim[i][1];
    }
    inp.close();
    
    TH1D histogram1("histogram1", "First Histogram", 100, 0, 100);
    TH1D histogram2("histogram2", "", 100, 0, 100);

    for (int i = 0; i < 100; i++)
    {
        histogram1.Fill(a2dim[i][0]);
    }

    for (int i = 0; i < 100; i++)
    {
        histogram2.Fill(a2dim[i][1]);
    } 

    histogram2.SetBinContent(40, 0);
    histogram2.Scale(2.1);

    TCanvas canvas("canvas", "Histogram of incrementing and random numbers");
    canvas.cd();
    canvas.SetGrid();

    histogram1.SetMinimum(histogram2.GetBinContent(histogram2.GetMinimumBin()) - 1);
    histogram1.SetMaximum(histogram2.GetBinContent(histogram2.GetMaximumBin()) + 1);

    histogram1.SetLineColor(9);
    histogram1.SetLineStyle(7);
    histogram1.GetXaxis()->SetTitle("Zahlen");
    histogram1.GetYaxis()->SetTitle("Anzahl");
    histogram1.Draw();
    canvas.Update();

    histogram2.SetLineColor(2);
    histogram2.SetLineStyle(1);
    histogram2.Draw("SAME");
    canvas.Update();

    TPaveStats *stats = (TPaveStats *) histogram1.FindObject("stats");
    stats->SetOptStat(1112211);

    TLegend legend(0.13, 0.75, 0.34, 0.9);
    legend.AddEntry(&histogram1, "aufsteigende Zahlen");
    legend.AddEntry(&histogram2, "Zufallszahlen");
    legend.Draw("SAME");
    canvas.Update();
    canvas.SaveAs("histograms.pdf");
}

int main(){
    writeToFile();
    fileToHistogram();
    return 0;
}
