/*
 *  Statistische Methoden der Datenanalyse
 *  
 *  Übungsgruppe 1 / Wacker
 *
 *              Christian-Roman Gerhorst
 *              Matrikelnummer: 140173
 *
 *               Blatt 1 - Aufgabe 3
 *
*/

#include <iostream>
#include <fstream>
#include <string>

#include "TRandom3.h"

using namespace std;

void find(){
    ifstream in; 
    in.open("aufg3_Liste_Namen.txt");

    string names[51];
    TRandom3 random(0);

    for (int i=0; i < 51; i++){
        in >> names[i];
    }
    in.close();
    cout << names[random.Integer(50)-1] << endl;
}

int main(){
    find();
    return 0;
}
