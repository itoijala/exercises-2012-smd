/*
 *  Statistische Methoden der Datenanalyse
 *  
 *  Übungsgruppe 1 / Wacker
 *
 *              Christian-Roman Gerhorst
 *              Matrikelnummer: 140173
 *
 *               Blatt 1 - Aufgabe 1
*/

#include <iostream>

#include "TCanvas.h"
#include "TText.h"

using namespace std;

void helloConsole()
{
    cout << "Hello, world" << endl;
}

void helloCanvas()
{
    TCanvas hw("hw", "Hello World!", 1024, 768);
    TText text(0.4, 0.7, "Hello, world");

    hw.cd(); 
    text.Draw();
    hw.SaveAs("hello_world.pdf");
}

int main()
{
    helloConsole();
    helloCanvas();

    return 0;
}
