#include <iostream>
#include <cmath>

using namespace std;

double ex17a(double x_min, double x_max)
{
	TRandom *random = new TRandom();
	double rand = random->Rndm();
	double x = x_min + rand * x_max;
	return x;				
}

double ex17b(double tau)
{
//	Normierungskonstante N = 1 / tau.
//	Bestimmung durch Transformation der
//	Gleichverteilung.
	TRandom *randomb = new TRandom();
	double r = randomb->Rndm();
	double x = - tau * log(1 - r);	
	return x;			
}

double ex17c(int n , int N , double x_min , double x_max)
{
//	Bestimmung durch Transformation der
//	Gleichverteilung.

	TRandom *randomc = new TRandom();
	double r = randomc->Rndm();
	double help = r * (pow(x_max , 1-n)
                        -  pow(x_min , 1-n))
			+  pow(x_min , 1-n);
	double x = pow(help , 1 / (1 - n));
	return x;
}

double ex17e(double x_0)
{
// für x_0=0 und sigma = 1
	
	TRandom *randome = new TRandom();
	double u1, u2, v1, v2, s, x1, x2, r, theta;
	
	u1 = randome->Rndm();
	u2 = randome->Rndm();
	v1 = 2 * u1 - 1;
	v2 = 2 * u2 - 1;
	 s = v1 * v1 + v2 * v2;
	if (s >= 1){}
	else
	{
		    r = sqrt(s);
		theta = atan(v2 / v1);
		   v1 = r * cos(theta);
		   v2 = r * sin(theta);
		   x1 = v1 * sqrt(- 2 / s * log(s));
		   x2 = v2 * sqrt(- 2 / s * log(s));
	}
//	da nun gilt: x1 = x - x_0
//	=> x = x1 + x_0
	x1 = x1 + x_0;
	x2 = x2 + x_0;
	return x1;
//	Sigma fehlt noch
}

double f(double v)
{
	double pi = 2 * acos(0);

	double m = 1;             // kg
	double T = 1;             // K
	double k = 1.3806488e-23; // J/K

	double factor = 4 * pi * pow(m / (2 * pi * k * T), 1.5);
	double expFactor = m / (2 * k * T);

	return factor * v * v * exp(-expFactor * v * v);
}

double integrate(double v_m)
{
	// composite Simpson's rule
	int N = 10000;
	double h = v_m / N;

	double sum = f(0) + f(v_m);
	for (int i = 0; i < N; i++)
	{
		sum += 2 * f(2 * i * h) + 4 * f((2 * i - 1) * h);
	}
	return sum * h / 3;
}

void ex19c()
{
	double pi = 2 * acos(0);

	double m = 1;             // kg
	double T = 1;             // K
	double k = 1.3806488e-23; // J/K

	double v_big = 2 * sqrt(2 * k * T /(pi * m));
	double v_small = 0;
	double v_m = 0;

	double integral = 0;
	do
	{
		v_m = (v_big + v_small) / 2;
		integral = integrate(v_m);
		if (integral > 1.0 / 2.0)
		{
			v_big = v_m;
		}
		else
		{
			v_small = v_m;
		}

		cout << v_small << "    " << v_m << "    " << v_big << "    " << integral << endl;
	}
	while (fabs(integral - 1.0 / 2.0) > 1e-10);

	cout << endl << "median: " << v_m << endl;
}

#if defined(__MAKE__)

int main()
{
//	ex17();
	ex19c();
	return 0;
}

#endif
