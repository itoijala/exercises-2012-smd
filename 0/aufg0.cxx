#include "TF1.h"
#include "TCanvas.h"
#include "TGraph.h"

Double_t mysin(Double_t *x, Double_t *par){
    return sin(x[0]);
}

void aufg0 (){
    double pi = 4.0 * atan(1.0);

    TCanvas *c1 = new TCanvas("c1", "c1", 700, 800);
    c1->Divide(2,2);

    TF1 *func1 = new TF1("func1", mysin, 0.0, 2 * pi);
    c1->cd(1);

    func1->Draw();

    TF1 *func2 = new TF1("func2", "sin(x)", 0.0, 2 * pi);
    c1->cd(2);

    func2->Draw();

   TF1 *func3 = new TF1("func3", "cos(x)", 0.0, 2 * pi);
   func3->Draw("SAME");
   const Int_t nn=100;
   Double_t x[nn], y[nn];

   for(int i=0; i<nn; i++){
       x[i] = 2.0 * pi * double(i) / double(nn);
       y[i] = sin(x[i]);
   }

   TGraph *g = new TGraph(nn,x,y);
   c1->cd(3);
   g->Draw("AC*");
}
