#include <iostream>
#include <cstdio>
#define _USE_MATH_DEFINES
#include <cmath>

#include "TVectorD.h"
#include "TMatrixD.h"
#include "TMath.h"
#include "TCanvas.h"
#include "TGraphErrors.h"
#include "TF1.h"
#include "TGraph.h"
#include "TAxis.h"

using namespace std;

TMatrixD *covarianceMatrix(TVectorD *sigma)
{
	TMatrixD *V = new TMatrixD(sigma->GetNrows(), sigma->GetNrows());
	for (int i = 0; i < sigma->GetNrows(); i++)
	{
		(*V)[i][i] = (*sigma)[i] * (*sigma)[i];
	}
	return V;
}

TMatrixD *designMatrix(TVectorD *x, int p, double (**f)(double))
{
	TMatrixD *A = new TMatrixD(x->GetNrows(), p);
	for (int i = 0; i < x->GetNrows(); i++)
	{
		for (int j = 0; j < p; j++)
		{
			(*A)[i][j] = (*f[j])((*x)[i]);
		}
	}
	return A;
}

void leastSquares(TMatrixD *A, TVectorD *y, TMatrixD *Vy, TVectorD *&a, TMatrixD *&Va)
{
	TMatrixD *W = new TMatrixD(TMatrixD::kInverted, *Vy);
	TMatrixD *At = new TMatrixD(TMatrixD::kTransposed, *A);
	TMatrixD *AtW = new TMatrixD(*At, TMatrixD::kMult, *W);
	TMatrixD *AtWA = new TMatrixD(*AtW, TMatrixD::kMult, *A);
	TMatrixD *AtWAi = new TMatrixD(TMatrixD::kInverted, *AtWA);
	TMatrixD *AtWAiAtW = new TMatrixD(*AtWAi, TMatrixD::kMult, *AtW);
	a = new TVectorD(*y);
	*a *= *AtWAiAtW;
	Va = AtWAi;
}

double chiSquared(TMatrixD *A, TVectorD *y, TMatrixD *V, TVectorD *a)
{
	TMatrixD *W = new TMatrixD(TMatrixD::kInverted, *V);
	TMatrixD *At = new TMatrixD(TMatrixD::kTransposed, *A);
	TVectorD *Wy = new TVectorD(*y);
	*Wy *= *W;
	TVectorD *AtWy = new TVectorD(*Wy);
	*AtWy *= *At;
	return *y * *Wy - *a * *AtWy;
}

double id(double x)
{
	return x;
}

double (*ids[])(double) = {id};

void ex35Work(size_t n, double *x_v, double *y_v, double *sigma_v, double &tau, double &sigma_tau, double &chi2)
{
	TVectorD *x = new TVectorD(n, &x_v[0]);
	TVectorD *y = new TVectorD(n, &y_v[0]);
	TVectorD *sigma = new TVectorD(n, &sigma_v[0]);

	TMatrixD *Vy = covarianceMatrix(sigma);
	TMatrixD *A = designMatrix(x, 1, ids);
	TVectorD *a;
	TMatrixD *Va;
	leastSquares(A, y, Vy, a, Va);
	tau = -1 / (*a)[0];
	sigma_tau = sqrt((*Va)[0][0]) * tau * tau;
	chi2 = chiSquared(A, y, Vy, a);
	cout << "tau / T:     (" << tau << " ± " << sigma_tau <<") s" << endl;
	cout << "Chi Squared: " << chi2 << endl;
	cout << "Probability: " << TMath::Prob(chi2, n - 1) << endl << endl;
}

double f1(double *x, double *p)
{
	return p[0] * exp(-x[0] / p[1]);
}

void ex35()
{
	size_t n = 14;
	cout << "Degrees of Freedom: " << n - 1 << endl << endl;

	const char *names[] = {"Erdinger", "Augustinerbrau", "Budweiser"};
	double h0[3];
	double *t = new double[n];
	double *h[3];
	double *sigma[3];
	for (size_t i = 0; i < 3; i++)
	{
		h[i] = new double[n];
		sigma[i] = new double[n];
	}

	FILE *file = fopen("data.txt", "r");
	for (size_t i = 0; i < 3; i++)
	{
		fscanf(file, "%lf", &h0[i]);
	}
	for (size_t i = 0; i < n; i++)
	{
		fscanf(file, "%lf", &t[i]);
		for (size_t j = 0; j < 3; j++)
		{
			fscanf(file, "%lf %lf", &h[j][i], &sigma[j][i]);
		}
	}
	fclose(file);

	TCanvas *canvas = new TCanvas("canvas");
	canvas->Divide(2, 2);

	for (size_t i = 0; i < 3; i++)
	{
		TGraphErrors *points = new TGraphErrors(n, t, h[i], NULL, sigma[i]);
		points->SetPoint(n, 0, h0[i]);
		points->SetPointError(n, 0, 0);
		points->SetTitle(names[i]);
		points->GetXaxis()->SetTitle("t / s");
		points->GetYaxis()->SetTitle("h / cm");
		canvas->cd(i + 1);
		points->Draw("A*");
		cout << names[i] << endl << endl;

		double *h1_v = new double[n];
		double *sigma1_v = new double[n];
		double *h2_v = new double[n];
		double *sigma2_v = new double[n];
		for (size_t j = 0; j < n; j++)
		{
			double h_norm = h[i][j] / h0[i];
			double sigma_norm = sigma[i][j] / h0[i];
			h1_v[j] = log(h_norm);
			sigma1_v[j] = sigma_norm / h_norm;
			h2_v[j] = h_norm - 1;
			sigma2_v[j] = sigma_norm;
		}

		double tau;
		double sigma_tau;
		double chi2;
		cout << "exponential" << endl;
		ex35Work(n, t, h1_v, sigma1_v, tau, sigma_tau, chi2);
		TF1 *f = new TF1("f", f1, 0, 400, 2);
		f->SetParameter(0, h0[i]);
		f->SetParameter(1, tau);
		TGraph *graph = new TGraph(f);
		graph->Draw("C");

		cout << "linear" << endl;
		ex35Work(n, t, h2_v, sigma2_v, tau, sigma_tau, chi2);
	}

	canvas->Update();
#if defined(__MAKE__)
	canvas->SaveAs("35.pdf");
#endif
}

#if defined(__MAKE__)

int main()
{
	ex35();
	return 0;
}

#endif
