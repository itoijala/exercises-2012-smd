#include <iostream>
#include <cmath>

#include "TRandom.h"
#include "TRandom3.h"
#include "TCanvas.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TH3D.h"
#include "TPaveStats.h"

using namespace std;

class Random
{
	private:
		int a;
		int b;
		int m;
		int previous;

	public:
		Random(int start = 65539)
		{
			a = 1601;
			b = 3456;
			m = 10000;
			previous = start;
		}

		double random()
		{
			previous = (a * previous + b) % m;
			return previous / (double) m;
		}
};

void ex11()
{
	Random *random1 = new Random();
	TRandom *random2 = new TRandom();
	TH1D *hist11 = new TH1D("hist11", "Random", 20, 0, 1);
	TH1D *hist12 = new TH1D("hist12", "TRandom", 20, 0, 1);
	TH2D *hist21 = new TH2D("hist21", "Random", 20, 0, 1, 20, 0, 1);
	TH2D *hist22 = new TH2D("hist22", "Random", 20, 0, 1, 20, 0, 1);
	TH3D *hist31 = new TH3D("hist31", "Random", 20, 0, 1, 20, 0, 1, 20, 0, 1);
	TH3D *hist32 = new TH3D("hist32", "Random", 20, 0, 1, 20, 0, 1, 20, 0, 1);

	double last11 = random1->random();
	double last12 = random2->Rndm();
	double last21 = random1->random();
	double last22 = random2->Rndm();
	for (int i = 0; i < 10000; i++)
	{
		double now1 = random1->random();
		double now2 = random2->Rndm();
		hist11->Fill(now1);
		hist12->Fill(now2);
		hist21->Fill(last12, now1);
		hist22->Fill(last22, now2);
		hist31->Fill(last11, last12, now1);
		hist32->Fill(last21, last22, now2);
		last11 = last12;
		last21 = last22;
		last12 = now1;
		last22 = now2;
	}

	TCanvas *canvas1 = new TCanvas("canvas1", "");
	canvas1->Divide(2, 1);
	canvas1->cd(1);
	hist11->Draw();
	canvas1->cd(2);
	hist12->Draw();

	TCanvas *canvas2 = new TCanvas("canvas2", "");
	canvas2->Divide(2, 1);
	canvas2->cd(1);
	hist21->Draw();
	canvas2->cd(2);
	hist22->Draw();

	TCanvas *canvas3 = new TCanvas("canvas3", "");
	canvas3->Divide(2, 1);
	canvas3->cd(1);
	hist31->Draw();
	canvas3->cd(2);
	hist32->Draw();

	/*
	 * The 1D and 3D histograms have empty areas (for all tested starting values).
	 * The problem ist also present in the 2D histograms but it is not so obvious.
	 * TRandom does not seem to have this problem.
	 */

#if !defined(__CINT__)
     canvas1->SaveAs("11-1.pdf");
     canvas2->SaveAs("11-2.pdf");
     canvas3->SaveAs("11-3.pdf");
#endif
}

void ex12()
{
}

void ex13(int n)
{
	/*
	 * Geburtstage
	 *
	 * Erste Annahme:	Das Jahr hat 365 Tage
	 * Zweite Annahme:	Die Geburtstage sind statistisch unabhängig
	 * 					und gleichverteilt.
	 * Dritte Annahme:	Der Jahrgang wird vernachlässigt.
	 *
	 * Erster Schritt:	Die Anzahl aller möglichen Verteilungsfälle m:
	 *					Ein Mensch hat die "Wahl" von 365 Tagen.
	 *					Die Möglichkeiten werden für n Menschen miteinander
	 *					multipliziert.
	 *					=> m = (365)^n .
	 *
	 * Zweiter Schritt:	Die Möglichkeiten u, nur an unterschiedlichen Tagen
	 *					Geburtstag zu haben:
	 *					Der erste hat 365 zur Wahl, der nächste nur 364, usw.
	 *					=> u = 365 * 364 * ... * (365 - (n - 1)) .
	 *
	 * Dritter Schritt: Wahrscheinlichkeit, dass n Personen nicht an gleichen
	 *					Tagen Geburtstag haben:
	 *					Aus den ersten beiden Schritten folgt
	 *					p(n) = u / m = (365 * ... * (365 - (n - 1))) / (365)^n .
	 *					
	 * Vierter Schritt:	Die Wahrscheinlichkeit, dass mind. zwei Personen
	 *					am gleichen Tag Geburtstag haben:
	 *					Es ergibt sich die Wahrscheinlichkeit, indem von der
	 *					Gesamtwahrscheinlichkeit der dritte Schritt subtrahiert
	 *					wird.
	 *					=> P(n) = 1 - p(n)
	 *							= 1 - (365 * ... * (365 - (n - 1))) / (365)^n .
	 *
	 *					Dies ist die gesuchte Wahrscheinlichkeit, dass zwei oder
	 *					mehr Menschen am gleichen Tag Geburtstag haben.
	 *
	 * Fünfter Schritt:	Die Personenanzahl n, damit P(n) >= 0,5 ist:
	 *					P(23) = 0,5073 > 0,5 .
	 *
	 *					Bei einer Personengruppe von mind. 23 Personen ist die
	 *					Wahrscheinlichkeit, dass zwei oder mehr Personen an einem
	 *					Tag Geburtstag feiern, größer als 50%.
	 */

	double P;
	double a = 1.0;
	double b = 1.0;
	for (double i = 365.0 ; i >= 365.0 - (n - 1.0) ; i--)
	{
		a *= i;	
	}
	for (double i = 1.0 ; i <= n ; i++)
	{
		b *= 365.0;	
	}
	P = (1.0 - (a / b)) * 100.0;
	cout << P << "%" << endl;
}

void ex14()
{
	/*
	 * a) 	Er sollte in jedem Fall wechseln
	 * b) 	Die Wahrscheinlichkeit beträgt 2 / 3 = 66,6666%, dass er den
	 *		Goldklumpen gewinnt.
	 * 		Für die Begründung siehe Anhang. Die Tabelle gibt alle Kombinationen an,
	 *		die möglich sind. Es zeigt sich, dass für den Fall, dass das Tor behalten wird,
	 *		sich hinter dem nur in 1 von 3 Fällen das Gold befindet. Für den Wechselfall
	 *		wird das Gold in 2 von 3 Fällen gewonnen.
	*/
}

void ex15()
{
    /*
     * a)
     * A = 1.8e4 / 3.7 * ((10000 GeV)**3.7 - (10 GeV)**3.7) * 1 / GeV**3.7 * 1 / (m² s sr)
     * A(x) = 1.8e4 / 3.7 * E**(3.7) in (per GeV**3.7 cm²  s  sr)
     */
    int events = 100000;
    double *energy = new double[events];
    TRandom3 random(0);
    for (int i = 0; i < events; i++)
    {
       energy[i] = pow(random.Rndm() * (pow(10000, 3.7) - pow(10, 3.7)) + pow(10, 3.7), 1 / 3.7);
    }

    /*
     * b)
     */
    TCanvas *canvas15 = new TCanvas("canvas15", "Random Energies");
    TH1D *histE = new TH1D("Statistics", "Differential Flux with Random Energies", 20, 10, 10000);

    for (int i = 0; i < events; i++)
    {
        histE->Fill(energy[i]);
    }

    canvas15->cd();
    histE->GetXaxis()->SetTitle("Energies / GeV");
    histE->GetYaxis()->SetTitle("Differential Flux / m m s sr GeV");
    gPad->SetLogx();
    gPad->SetLogy();
    histE->Draw();
    gPad->Update();
    TPaveStats *st = (TPaveStats*)histE->FindObject("stats");
    st->SetX1NDC(0.18);
    st->SetX2NDC(0.35);
    st->SetY1NDC(0.78);
    st->SetY2NDC(0.88);
    canvas15->Update();

    /*
     * c)
     * fraction has to be normed at 1, for cosmic radiation to remain equal, is that right?
     * t = 1e6 / 0.2 / 6.285 = 9.21 days
     */

#if !defined(__CINT__)
     canvas15->SaveAs("RandomEnergies.pdf");
#endif
}

#if !defined(__CINT__)

int main()
{
	ex11();
	ex15();
	return 0;
}

#endif
