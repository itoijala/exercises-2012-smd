#include <TStyle.h>
#include <TCanvas.h>
#include <TGraph.h>
#include <TRandom.h>
#include <TLine.h>
#include <TLegend.h>
#include <TLegendEntry.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <TH1.h>
#include <TH2.h>

void aufg28()

{
///////////////////////////////////////////////////////////////////////////////////////
// Im Folgenden ist lamda durch t ersetzt!!!!
//////////////////////////////////////////////////////////////////////////////////////////

TCanvas *c1 = new TCanvas("c1","c1",700,700);
TCanvas *c2 = new TCanvas("c2","c2",900,900);
TCanvas *c3 = new TCanvas("c3","c3",900,900);
 c2->Divide(2,2);

 const double tmin = -25.0, tmax = 25.0; // t entspricht lamda
 const int tbin = 200;

 TH2F *hxy = new TH2F("hxy","x vs y", tbin, tmin, tmax, tbin, tmin, tmax);
 TH1F *ht = new TH1F("ht","Fisher t", tbin, tmin, tmax);
 TH1F *ht0 = new TH1F("ht0","Fisher t H0", tbin, tmin, tmax);
 TH1F *ht1 = new TH1F("ht1","Fisher t H1", tbin, tmin, tmax);
 TH1F *hx = new TH1F("hx","x", tbin, tmin, tmax);
 TH1F *hx0 = new TH1F("hx0","x H0", tbin, tmin, tmax);
 TH1F *hx1 = new TH1F("hx1","x H1", tbin, tmin, tmax);
 TH1F *hy = new TH1F("hy","y", tbin, tmin, tmax);
 TH1F *hy0 = new TH1F("hy0","y H0", tbin, tmin, tmax);
 TH1F *hy1 = new TH1F("hy1","y H1", tbin, tmin, tmax);

 /////////////////////////////////////////// Bitte eintragen /////////////////////////////////
 Int_t nev1=; //hier ist die Anzahl der Population 1 einzutragen
 Int_t nev0=; //hier ist die Anzahl der Population 0 einzutragen
 Int_t i,j,k;

 // Parameter der Hypothesen
 // Leerstellen zwischen { } bitte auffüllen!
 ///////////////////////////////////// Bitte eintragen ////////////////////////////////
 Double_t mu0[2]={ , };   // Mittelwerte x, y H0
 Double_t mu1[2]={ , };   // Mittelwerte x, y H1
 Double_t V0[4]={ , , ,}; // Kovarianzmatrix H0
 Double_t V1[4]={ , , , };// Kovarianzmatrix H1
 // 2x2-Matrizen sind als Vektor dargestellt: M_ij = M[2*i+j], i,j=0,1
 // W stellt die komibinierte Kovarianzmatrix dar
 Double_t W[4],Wi[4],b[2],a[2],zx,zy,x,y,t; 

 double pi = 4.0 * atan(1.0);
 double pih = 0.5*pi;

 // Für die Fisher-Diskriminante
 for(i=0; i<2; i++) {
     b[i] = mu0[i] - mu1[i]; // Distanz D
     for(j=0; j<2; j++) {
	 W[2*i+j] = V0[2*i+j] + V1[2*i+j];  // W stellt die komibinierte Kovarianzmatrix dar für den Fall gleicher Anzahl von Ereignissen in den Populationen
     }
 }

 // W invers:
 Double_t det = W[0]*W[3] - W[1]*W[2];
 Wi[0] = W[3]/det;
 Wi[1] = -W[2]/det;
 Wi[2] = -W[1]/det;
 Wi[3] = W[0]/det;

 // einige Konsolenausgaben
 cout << "b: " << setw(7) << b[0] << setw(7) << b[1] << endl;
 cout << "W: " << setw(7) << W[0] << setw(7) << W[1] << endl;
 cout << "W: " << setw(7) << W[2] << setw(7) << W[3] << endl;
 cout << "det: " << setw(7) << det << endl;
 cout << "Wi: " << setw(7) << Wi[0] << " " << setw(7) << Wi[1] << endl;
 cout << "Wi: " << setw(7) << Wi[2] << " " << setw(7) << Wi[3] << endl;

 // W^-1 * b = lamda
 // b bezeichnet die Distanz D (Abstand Schwerounkte)
 for(i=0; i<2; i++) {
     a[i] = 0; //lamda
     for(j=0; j<2; j++) {
	 a[i] += Wi[2*i+j] * b[j]; //hier wurd lamda berechnet
     }
 }

 cout << "a: " << setw(7) << a[0] << " " << setw(7) << a[1] << endl;

 Double_t R[3];
 //////////////////////// Bitte einen geeigneten tcut-Parameter eintragen (z.B. Wert für maximale Reinheit), dafür das Programm natürlich erst einmal durchlaufen ;-) /////
 //////////////////////// xlo und xhi auch geeignet anpassen - Achsenbereich ///////////////////////////////////////////
 Double_t tcut = 1.5, xlo=0, xhi=10;
 Int_t nn,np;

 // ---------------  H0 ----------------------

 // Für die möglicherweise korrelierte Gaußverteilung
 R[0] = sqrt(V0[0]);  // R11
 R[1] = V0[1]/R[0];   // R12
 R[2] = sqrt(V0[3] - R[1]*R[1]);

 cout << "R: " << setw(7) << R[0] << endl;
 cout << "R: " << setw(7) << R[1] << " " << setw(7) << R[2] << endl;

	
 nn=np=0;
 

// Hier werden Zufallszahlen der Population P0 erzeugt	
 for(k=0; k<nev0; k++) {
     gRandom->Rannor(zx,zy);
     x = mu0[0] + R[0] * zx;
     y = mu0[1] + R[1] * zx + R[2] * zy;
     t = a[0]*x + a[1]*y;
     if(t<tcut) {nn++;} else {np++;}
     hxy->Fill(x,y);
     hx->Fill(x);
     hx0->Fill(x);
     hy->Fill(y);
     hy0->Fill(y);
     ht->Fill(t);
     ht0->Fill(t);
 }
 cout << "H0: " << nn << " (" << nn*100.0/nev0 << "%)  t<" << tcut;
 cout << ", " << np << " (" << np*100.0/nev0 << "%)  t>" << tcut << endl;

 // ---------------  H1 ----------------------

 // Für die möglicherweise korrelierte Gaußverteilung
 R[0] = sqrt(V1[0]);  // R11
 R[1] = V1[1]/R[0];   // R12
 R[2] = sqrt(V1[3] - R[1]*R[1]);

 cout << "R: " << setw(7) << R[0] << endl;
 cout << "R: " << setw(7) << R[1] << " " << setw(7) << R[2] << endl;


 nn=np=0;

// Hier werden Zufallszahlen der Population P1 erzeugt
 for(k=0; k<nev1; k++) {
   gRandom->Rannor(zx,zy);
   x = mu1[0] + R[0] * zx;
   y = mu1[1] + R[1] * zx + R[2] * zy;
   t = a[0]*x + a[1]*y;
   if(t<tcut) {nn++;} else {np++;}
   hxy->Fill(x,y);
   hx->Fill(x);
   hx1->Fill(x);
   hy->Fill(y);
   hy1->Fill(y);
   ht->Fill(t);
   ht1->Fill(t);
 }
 cout << "H1: " << nn << " (" << nn*100.0/nev1 << "%)  t<" << tcut;
 cout << ", " << np << " (" << np*100.0/nev1 << "%)  t>" << tcut << endl;
 c1->cd();

 // Fisher als Linie im 2d-Histogramm
  TLine *line = new TLine(xlo, (tcut - a[0]*xlo)/a[1], 
			 xhi, (tcut - a[0]*xhi)/a[1]);
 hxy->Draw();
 line->Draw();
 c2->cd(1);
 ht->Draw();
 ht0->Draw("SAME");
 ht1->Draw("SAME");
 // Double_t xmin, ymin, xmax, ymax;
 // gPad -> GetRange(xmin, ymin, xmax, ymax);
 // cout << xmin << ymin << xmax << ymax << endl;
 TLine *cut = new TLine(tcut,0.0, tcut, ht->GetMaximum());
 cut -> SetLineStyle(2); // dashed
 cut -> Draw();
 c2->cd(3);
 hx->Draw();
 hx0->Draw("SAME");
 hx1->Draw("SAME");
 c2->cd(4);
 hy->Draw();
 hy0->Draw("SAME");
 hy1->Draw("SAME");
 c2->cd(2);

 // Vorbereitungen für Teile d, e, f

 Double_t H0cumul[tbin+1], H1cumul[tbin+1],tcumul[tbin+1];
 H0cumul[0] = 0.;
 H1cumul[0] = 0.;
 tcumul[0] = ht0->GetBinLowEdge(1);

 for(int i=1; i<=tbin; i++) {
     H0cumul[i] =  H0cumul[i-1] + ht0->GetBinContent(i);
     H1cumul[i] =  H1cumul[i-1] + ht1->GetBinContent(i);
     tcumul[i] = ht0->GetBinLowEdge(i) + ht0->GetBinWidth(i);
 }
 Double_t norm=H0cumul[tbin], norm1=H1cumul[tbin];
 cout << "norm: " << norm << endl;
 for(int i=0; i<=tbin; i++) {
     H0cumul[i] = 1.0 - H0cumul[i]/norm;
     H1cumul[i] = (norm1 - H1cumul[i])/norm;
 }

 ////////////////////////////////// Ab hier dürfen Sie weitermachen!

}
