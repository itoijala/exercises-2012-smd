#include <iostream>
#define _USE_MATH_DEFINES
#include <cmath>

#include "TRandom.h"
#include "TRandom3.h"
#include "TCanvas.h"
#include "TH2D.h"
#include "TGraph.h"

using namespace std;

double randomNormalGauss(TRandom *random)
{
	static double cache = 0;
	static bool isCached = false;

	if (isCached)
	{
		isCached = false;
		return cache;
	}
	double v1 = 2 * random->Rndm() - 1;
	double v2 = 2 * random->Rndm() - 1;
	double s = v1 * v1 + v2 * v2;
	if (s >= 1)
	{
		return randomNormalGauss(random);
	}
	double x1 = v1 * sqrt(-2 / s * log(s));
	double x2 = v2 * sqrt(-2 / s * log(s));
	isCached = true;
	cache = x2;
	return x1;
}

double randomGauss(TRandom *random, double mu, double sigma)
{
	return sigma * randomNormalGauss(random) + mu;
}

void randomCorrelatedGauss(TRandom *random, double mu1, double sigma1, double mu2, double sigma2, double correlation, double &r1, double &r2)
{
	double n1 = randomNormalGauss(random);
	double n2 = randomNormalGauss(random);
	r1 = sqrt(1 - correlation * correlation) * sigma1 * n1 + correlation * sigma2 * n2 + mu1;
	r2 = sigma2 * n2 + mu2;
}

template <int hack> // work around bug in ACLiC
void meanCovarianceCorrelation(double *data1, double *data2, size_t length, double (&mean)[2], double (&covariance)[2][2], double &correlation)
{
	mean[0] = 0;
	mean[1] = 0;
	double M1 = 0;
	double M2 = 0;
	double C = 0;

	for (size_t i = 0; i < length; i++)
	{
		double delta1 = data1[i] - mean[0];
		double delta2 = data2[i] - mean[1];
		mean[0] += delta1 / (i + 1);
		mean[1] += delta2 / (i + 1);
		M1 += delta1 * (data1[i] - mean[0]);
		M2 += delta2 * (data2[i] - mean[1]);
		C += (data1[i] - mean[0]) * delta2;
	}

	covariance[0][0] = M1 / (length - 1);
	covariance[1][1] = M2 / (length - 1);
	covariance[0][1] = covariance[1][0] = C / length;
	correlation = C / (sqrt(M1) * sqrt(M2));
}

template <size_t length>
void vectorSubtract(double (&v1)[length], double (&v2)[length], double (&r)[length])
{
	for (size_t i = 0; i < length; i++)
	{
		r[i] = v1[i] - v2[i];
	}
}

template <size_t rows, size_t cols>
void matrixAdd(double (&m1)[rows][cols], double (&m2)[rows][cols], double (&r)[rows][cols])
{
	for (size_t i = 0; i < rows; i++)
	{
		for (size_t j = 0; j < cols; j++)
		{
			r[i][j] = m1[i][j] + m2[i][j];
		}
	}
}

template <int hack>
void matrix2Inverse(double (&m)[2][2], double (&r)[2][2])
{
	double det = m[0][0] * m[1][1] - m[1][0] * m[0][1];
	r[0][0] = m[1][1] / det;
	r[0][1] = -m[0][1] / det;
	r[1][0] = -m[1][0] / det;
	r[1][1] = m[0][0] / det;
}

template <size_t rows, size_t cols>
void matrixMultiply(double (&m)[rows][cols], double (&v)[cols], double (&r)[rows])
{
	for (size_t i = 0; i < rows; i++)
	{
		r[i] = 0;
		for (size_t j = 0; j < cols; j++)
		{
			r[i] += m[i][j] * v[j];
		}
	}
}

template <size_t length>
double vectorMultiply(double (&v1)[length], double (&v2)[length])
{
	double sum = 0;
	for (size_t i = 0; i < length; i++)
	{
		sum += v1[i] * v2[i];
	}
	return sum;
}

template <int hack>
void fisherDiscriminant(double *dataX, double *dataY, size_t length, double (&a)[2], double *lambda)
{
	for (size_t i = 0; i < length; i++)
	{
		double d[2] = {dataX[i], dataY[i]};
		lambda[i] = vectorMultiply<2>(a, d);
	}
}

int getMaximumIndex(double *data, size_t length)
{
	double max = data[0];
	int index = 0;
	for (size_t i = 1; i < length; i++)
	{
		if (data[i] > max)
		{
			max = data[i];
			index = i;
		}
	}
	return index;
}

void discrimantAnalysis()
{
	/*
	 * a
	 */
	TRandom *random = new TRandom3();
	size_t events = 1e4;
	double *x0 = new double[events];
	double *y0 = new double[events];
	double *x1 = new double[events];
	double *y1 = new double[events];

	for (size_t i = 0; i < events; i++)
	{
		randomCorrelatedGauss(random, 5.5, 0.8, 1.0, 0.8, 0.0, x0[i], y0[i]);
		randomCorrelatedGauss(random, 5.0, 2.5, 5.0, 2.5, 0.3, x1[i], y1[i]);
	}

	/*
	 * b
	 */
	TCanvas *canvas = new TCanvas("canvasb");
	canvas->cd();
	TH2D *hist0 = new TH2D("Populations", "", 100, -5, 15, 100, -5, 15);
	TH2D *hist1 = new TH2D("hist1", "", 100, -5, 15, 100, -5, 15);
	for (size_t i = 0; i < events; i++)
	{
		hist0->Fill(x0[i], y0[i]);
		hist1->Fill(x1[i], y1[i]);
	}
	hist0->GetXaxis()->SetTitle("x");
	hist0->GetYaxis()->SetTitle("y");
	hist0->SetMarkerColor(2);
	hist0->SetTitle("Signal and Background");
	hist0->Draw();
	hist1->Draw("SAME");

	canvas->Update();

#if defined(__MAKE__)
	canvas->SaveAs("b.pdf");
#endif

	/*
	 * c, d
	 */
	double mean0[2];
	double covariance0[2][2];
	double correlation0;
	double mean1[2];
	double covariance1[2][2];
	double correlation1;

	meanCovarianceCorrelation<0>(x0, y0, events, mean0, covariance0, correlation0);
	meanCovarianceCorrelation<0>(x1, y1, events, mean1, covariance1, correlation1);

	printf("0: Mean:                    ( %- .10f %- .10f )\n", mean0[0], mean0[1]);
	printf("   Covariance-Matrix:       ⎛ %- .10f %- .10f ⎞\n", covariance0[0][0], covariance0[0][1]);
	printf("                            ⎜                             ⎟\n");
	printf("                            ⎝ %- .10f %- .10f ⎠\n", covariance0[1][0], covariance0[1][1]);
	printf("   Correlation-Coefficient:   %- .10f\n", correlation0);
	printf("1: Mean:                    ( %- .10f %- .10f )\n", mean1[0], mean1[1]);
	printf("   Covariance-Matrix:       ⎛ %- .10f %- .10f ⎞\n", covariance1[0][0], covariance1[0][1]);
	printf("                            ⎜                             ⎟\n");
	printf("                            ⎝ %- .10f %- .10f ⎠\n", covariance1[1][0], covariance1[1][1]);
	printf("   Correlation-Coefficient:   %- .10f\n", correlation1);

	/*
	 * e
	 */
	double W[2][2];
	double Winverse[2][2];
	double a[2];
	double b[2];

	vectorSubtract<2>(mean0, mean1, b);
	matrixAdd<2, 2>(covariance0, covariance1, W);
	matrix2Inverse<0>(W, Winverse);
	matrixMultiply<2, 2>(Winverse, b, a);

	double *lambda0 = new double[events];
	double *lambda1 = new double[events];
	fisherDiscriminant<0>(x0, y0, events, a, lambda0);
	fisherDiscriminant<0>(x1, y1, events, a, lambda1);

	/*
	 * f
	 */
	size_t bins = 100;
	double minBin = -7;
	double maxBin = 7;
	canvas = new TCanvas("canvasf");
	canvas->Divide(1, 3);

	TH1D *histLambda0 = new TH1D("histLambda0", "lambda 0", bins, minBin, maxBin);
	histLambda0->FillN(events, lambda0, NULL);
	canvas->cd(1);
	histLambda0->Draw();

	TH1D *histLambda1 = new TH1D("histLambda1", "lambda 1", bins, minBin, maxBin);
	histLambda1->FillN(events, lambda1, NULL);
	canvas->cd(2);
	histLambda1->Draw();

	TH1D *histLambda = new TH1D("histLambda", "lambda Sum", bins, minBin, maxBin);
	histLambda->Add(histLambda0, histLambda1);
	canvas->cd(3);
	histLambda->Draw();

	canvas->Update();

#if defined(__MAKE__)
	canvas->SaveAs("f.pdf");
#endif

	/*
	 * g
	 */
	double *lambdaX = new double[bins];
	double *cumulLambda0 = new double[bins];
	double *cumulLambda1 = new double[bins];
	double *cumulLambda = new double[bins];

	lambdaX[0] = minBin + (minBin + maxBin) / 2;
	cumulLambda0[0] = histLambda0->GetBinContent(1);
	cumulLambda1[0] = histLambda1->GetBinContent(1);
	cumulLambda[0] = histLambda->GetBinContent(1);
	for (size_t i = 1; i < bins; i++)
	{
		lambdaX[i] = minBin + i * (maxBin - minBin) / bins + (minBin + maxBin) / 2;
		cumulLambda0[i] = cumulLambda0[i - 1] + histLambda0->GetBinContent(i + 1);
		cumulLambda1[i] = cumulLambda1[i - 1] + histLambda1->GetBinContent(i + 1);
		cumulLambda[i] = cumulLambda[i - 1] + histLambda->GetBinContent(i + 1);
	}

	double *purity = new double[bins];
	double *efficiency = new double[bins];

	for (size_t i = 0; i < bins; i++)
	{
		purity[i] = cumulLambda0[i] / cumulLambda[i];
		efficiency[i] = cumulLambda0[i] / events;
	}

	canvas = new TCanvas("canvasg");
	canvas->Divide(1, 2);

	TGraph *graphPurity = new TGraph(bins, lambdaX, purity);
	graphPurity->GetXaxis()->SetTitle("lambda");
	graphPurity->GetYaxis()->SetTitle("Purity");
	graphPurity->SetTitle("Purity");
	canvas->cd(1);
	graphPurity->Draw("AL");

	TGraph *graphEfficiency = new TGraph(bins, lambdaX, efficiency);
	graphEfficiency->GetXaxis()->SetTitle("lambda");
	graphEfficiency->GetYaxis()->SetTitle("Efficiency");
	graphEfficiency->SetTitle("Efficiency");
	canvas->cd(2);
	graphEfficiency->Draw("AL");

	canvas->Update();

#if defined(__MAKE__)
	canvas->SaveAs("g.pdf");
#endif

	/*
	 * h
	 */
	double *ratio = new double[bins];

	for (size_t i = 0; i < bins; i++)
	{
		ratio[i] = cumulLambda0[i] / cumulLambda1[i];
	}

	canvas = new TCanvas("canvash");
	canvas->Divide(1, 2);

	TGraph *graphRatio= new TGraph(bins, lambdaX, ratio);
	graphRatio->GetXaxis()->SetTitle("lambda");
	graphRatio->GetYaxis()->SetTitle("S / B");
	graphRatio->SetTitle("Ratio");
	canvas->cd(1);
	graphRatio->Draw("AL");

	cout << "Ratio Maximum at lambda = " << lambdaX[getMaximumIndex(ratio, bins)] << endl;

	/*
	 * i
	 */
	double *significance = new double[bins];

	for (size_t i = 0; i < bins; i++)
	{
		significance[i] = cumulLambda0[i] / sqrt(cumulLambda0[i] + cumulLambda1[i]);
	}

	TGraph *graphSignificance= new TGraph(bins, lambdaX, significance);
	graphSignificance->GetXaxis()->SetTitle("lambda");
	graphSignificance->GetYaxis()->SetTitle("Significance");
	graphSignificance->SetTitle("Significance");
	canvas->cd(2);
	graphSignificance->Draw("AL");

	canvas->Update();

	cout << "Significance Maximum at lambda = " << lambdaX[getMaximumIndex(significance, bins)] << endl;

#if defined(__MAKE__)
	canvas->SaveAs("h.pdf");
#endif
}

#if defined(__MAKE__)

int main()
{
	discrimantAnalysis();
	return 0;
}

#endif
