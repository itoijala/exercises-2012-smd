#include <iostream>
#include <cstdio>
#define _USE_MATH_DEFINES
#include <cmath>

#include "TVectorD.h"
#include "TMatrixD.h"
#include "TF1.h"
#include "TGraph.h"
#include "TCanvas.h"
#include "TAxis.h"

using namespace std;

void weightedMean(double *data, double *stds, size_t length, double &mean, double &error)
{
	double sumWeights = 0;
	mean = 0;
	for (size_t i = 0; i < length; i++)
	{
		double weight = 1 / (stds[i] * stds[i]);
		sumWeights += weight;
		mean += data[i] * weight;
	}
	mean /= sumWeights;
	error = 1 / sqrt(sumWeights);
}

void ex29a()
{
	size_t n = 3;
	double c[] = {299798000,
	              299789000,
	              299797000};
	double cError[] = {5000,
	                   4000,
	                   8000};

	double mean, error;
	weightedMean(&c[0], &cError[0], n, mean, error);
	printf("c = (%.0f +- %0.f) m/s\n", mean, error);
}

void ex29b()
{
	size_t n = 2;
	double N[] = {389,
	              423};
	double mean = 0;
	for (size_t i = 0; i < n; i++)
	{
		mean += N[i];
	}
	mean /= n;

	/*
	 * This is like one 2 minute experiment with the result N1 + N2.
	 * N is Poisson distributed.
	 */
	double error = sqrt(mean);
	printf("N = %.0f +- %.0f\n", mean, error);
}

TMatrixD *designMatrix(TVectorD *x, int p, double (**f)(double))
{
	TMatrixD *A = new TMatrixD(x->GetNrows(), p);
	for (int i = 0; i < x->GetNrows(); i++)
	{
		for (int j = 0; j < p; j++)
		{
			(*A)[i][j] = (*f[j])((*x)[i]);
		}
	}
	return A;
}

void leastSquares(TMatrixD *A, TVectorD *y, double sigma, TVectorD *&a, TMatrixD *&V)
{
	TMatrixD *At = new TMatrixD(TMatrixD::kTransposed, *A);
	TMatrixD *AtA = new TMatrixD(TMatrixD::kAtA, *A);
	TMatrixD *AtAi = new TMatrixD(TMatrixD::kInverted, *AtA);
	TMatrixD *AtAiAt = new TMatrixD(*AtAi, TMatrixD::kMult, *At);
	a = new TVectorD(*y);
	*a *= *AtAiAt;
	V = AtAi;
	*V *= sigma * sigma;
}

template <size_t length>
void degToRad(double (&a)[length])
{
	for (size_t i = 0; i < length; i++)
	{
		a[i] = M_PI / 180 * a[i];
	}
}

template <int n>
void ex30Work(double (&psiValues)[n], double (&alphaValues)[n], double sigma)
{
	TVectorD *psi = new TVectorD(n, &psiValues[0]);
	TVectorD *alpha = new TVectorD(n, &alphaValues[0]);

	double (*trig[])(double) = {cos,
	                            sin};

	TMatrixD *A = designMatrix(psi, 2, trig);

	TVectorD *a;
	TMatrixD *V;
	leastSquares(A, alpha, sigma, a, V);

	double sigma_a1 = sqrt((*V)[0][0]);
	double sigma_a2 = sqrt((*V)[1][1]);
	double rho = (*V)[0][1] / (sigma_a1 * sigma_a2);

	double A0 = sqrt(pow((*a)[0], 2) + pow((*a)[1], 2));
	double sigma_A0 = sqrt(1 / (pow((*a)[0], 2) + pow((*a)[1], 2)) * (pow((*a)[0] * sigma_a1, 2) + pow((*a)[1] * sigma_a2, 2)));
	double delta = atan((*a)[1] / (*a)[0]);
	double sigma_delta = sqrt(pow((*a)[1] / (pow((*a)[0], 2) + pow((*a)[1], 2)), 2) * pow(sigma_a1, 2) + pow((*a)[0] / (pow((*a)[0], 2) + pow((*a)[1], 2)), 2) * pow(sigma_a2, 2));

	cout << "Design Matrix" << endl;
	A->Print();
	cout << "Parameters" << endl;
	a->Print();
	cout << "Covariance Matrix" << endl;
	V->Print();
	printf("sigma_a1    = %- .10f\n", sigma_a1);
	printf("sigma_a2    = %- .10f\n", sigma_a2);
	printf("rho         = %- .10f\n", rho);
	printf("A0          = %- .10f\n", A0);
	printf("sigma_A0    = %- .10f\n", sigma_A0);
	printf("delta       = %- .10f\n", delta);
	printf("sigma_delta = %- .10f\n", sigma_delta);
}

void ex30()
{
	double psiValues[] = {  0,
	                       30,
	                       60,
	                       90,
	                      120,
	                      150,
	                      180,
	                      210,
	                      240,
	                      270,
	                      300,
	                      330};
	degToRad<12>(psiValues);
	double alphaValues[] = {-0.032,
	                         0.010,
	                         0.057,
	                         0.068,
	                         0.076,
	                         0.080,
	                         0.031,
	                         0.005,
	                        -0.041,
	                        -0.090,
	                        -0.088,
	                        -0.074};
	double sigma = 0.011;

	ex30Work<12>(psiValues, alphaValues, sigma);

	double psiValuesE[] = {  0,
	                        90,
	                       120,
	                       150,
	                       180,
	                       210,
	                       240,
	                       270,
	                       300,
	                       330};
	degToRad<10>(psiValuesE);
	double alphaValuesE[] = {-0.032,
	                          0.068,
	                          0.076,
	                          0.080,
	                          0.031,
	                          0.005,
	                         -0.041,
	                         -0.090,
	                         -0.088,
	                         -0.074};

	cout << endl << endl << "30 e" << endl << endl;
	ex30Work<10>(psiValuesE, alphaValuesE, sigma);
}

double poly0(double x)
{
	return 1;
}

double poly1(double x)
{
	return x;
}

double poly2(double x)
{
	return x * x;
}

double poly3(double x)
{
	return x * x * x;
}

double poly4(double x)
{
	return x * x * x * x;
}

double (*polys[])(double) = {poly0,
                             poly1,
                             poly2,
                             poly3,
                             poly4};

double poly(double *x, double *a)
{
	double sum = 0;
	for (size_t i = 0; i < 5; i++)
	{
		sum += a[i] * (*polys[i])(x[0]);
	}
	return sum;
}

double (*legendre0)(double) = poly0;

double legendre1(double x)
{
	x = (x - 9) / 2;
	return x;
}

double legendre2(double x)
{
	x = (x - 9) / 2;
	return 0.5 * (3 * x * x - 1);
}

double legendre3(double x)
{
	x = (x - 9) / 2;
	return 0.5 * (5 * x * x * x - 3 * x);
}

double legendre4(double x)
{
	x = (x - 9) / 2;
	return 0.125 * (35 * x * x * x * x - 30 * x * x + 3);
}

double (*legendres[])(double) = {legendre0,
                                 legendre1,
                                 legendre2,
                                 legendre3,
                                 legendre4};

double legendre(double *x, double *a)
{
	double sum = 0;
	for (size_t i = 0; i < 5; i++)
	{
		sum += a[i] * (*legendres[i])(x[0]);
	}
	return sum;
}

double chiSquared(TMatrixD *A, TVectorD *a, TVectorD *y)
{
	TVectorD *tmp = new TVectorD(*a);
	*tmp *= *A;
	TVectorD *chi = new TVectorD(*y);
	*chi -= *tmp;
	return chi->Norm2Sqr();
}

TMatrixD *correlation(TMatrixD *V)
{
	TMatrixD *rho = new TMatrixD(*V);
	for (int i = 0; i < V->GetNrows(); i++)
	{
		for (int j = 0; j < V->GetNcols(); j++)
		{
			if (i == j)
			{
				(*rho)[i][j] = 0;
			}
			else
			{
				(*rho)[i][j] = (*V)[i][j] / sqrt((*V)[i][i] * (*V)[j][j]);
			}
		}
	}
	return rho;
}

void ex31Work(size_t p, TVectorD *x, TVectorD *y, double sigma, double (**fs)(double), double (*f)(double *, double *), const char *name, const char* num)
{
	TMatrixD *A;
	TVectorD *a;
	TMatrixD *V;
	double chi2;
	TMatrixD *rho;

	A = designMatrix(x, p, fs);
	leastSquares(A, y, sigma, a, V);
	chi2 = chiSquared(A, a, y);
	rho = correlation(V);

	char *title = new char[12];
	sprintf(title, "%s %s", name, num);
	cout << endl << title << endl;
	cout << endl << "Parameters" << endl;
	a->Print();
	cout << endl << "Covariance Matrix" << endl;
	V->Print();
	cout << "chi^2 = " << chi2 << endl;
	cout << endl << "Correlation Coefficients" << endl;
	rho->Print();

	TF1 *f1 = new TF1("f", f, 7, 11, 5);
	for (size_t i = 0; i < 5; i++)
	{
		if (i < p)
		{
			f1->SetParameter(i, (*a)[i]);
		}
		else
		{
			f1->SetParameter(i, 0);
		}
	}
	TGraph *g = new TGraph(f1);
	g->SetTitle(title);
	g->SetMinimum(0.5);
	g->SetMaximum(3);
	g->GetXaxis()->SetLimits(7, 11);
	g->GetYaxis()->SetLimits(0.5, 3);
	g->Draw("AC");
}

void ex31()
{
	int n = 8;
	double xValues[] = { 7.250,
	                     7.750,
	                     8.250,
	                     8.750,
	                     9.250,
	                     9.750,
	                    10.250,
	                    10.750};
	double yValues[] = {0.883,
	                    0.770,
	                    0.804,
	                    0.832,
	                    1.069,
	                    1.660,
	                    1.802,
	                    2.566};
	double sigma = 0.1;
	TVectorD *x = new TVectorD(n, &xValues[0]);
	TVectorD *y = new TVectorD(n, &yValues[0]);

	TGraph *gpoints = new TGraph(n, xValues, yValues);

	for (size_t p = 1; p < 5; p++)
	{
		char *num = new char[2];
		sprintf(num, "%lu", p);
		char *file = new char[9];
		sprintf(file, "31_%s.pdf", num);
		TCanvas *canvas = new TCanvas("canvas");
		canvas->Divide(2, 1);

		canvas->cd(1);
		ex31Work(p, x, y, sigma, polys, poly, "Poly", num);

		canvas->cd(2);
		ex31Work(p, x, y, sigma, legendres, legendre, "Legendre", num);

		canvas->cd(1);
		gpoints->Draw("*");
		canvas->cd(2);
		gpoints->Draw("*");
		canvas->Update();

#if defined(__MAKE__)
		canvas->SaveAs(file);
#endif
	}
}

#if defined(__MAKE__)

int main()
{
	ex29a();
	ex29b();
	ex30();
	ex31();
	return 0;
}

#endif
