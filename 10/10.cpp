#include <iostream>
#include <cstdio>
#define _USE_MATH_DEFINES
#include <cmath>

#include "TRandom3.h"
#include "TCanvas.h"
#include "TH2D.h"

using namespace std;

/*
 * b
 */
template <int hack>
void trueEvent(double eps, double (&g)[2], double (&f)[2])
{
	f[0] = 1 / (1 - 2 * eps) * ((1 - eps) * g[0] - eps * g[1]);
	f[1] = 1 / (1 - 2 * eps) * ((-eps) * g[0] + (1 - eps) * g[1]);
}

/*
 * c
 */
template <int hack>
void covarianceMEC(double eps, double (&g)[2], double (&V)[2][2], double (&er_f)[2], double &corCoef)
{
	double detr = pow(1 - 2 * eps, -2);
	V[0][0] = detr * (pow(1 - eps, 2) * g[0] + pow(eps, 2) * g[1]);
	V[0][1] = -detr * (eps * (1 - eps) * (g[0] + g[1]));
	V[1][0] = V[0][1];
	V[1][1] = detr * (pow(eps, 2) * g[0] + pow(1 - eps, 2) * g[1]);

	er_f[0] = sqrt(V[0][0]);
	er_f[1] = sqrt(V[1][1]);
	corCoef = V[0][1] / sqrt(V[0][0] * V[1][1]);
}

template <int hack>
void printResult(double (&f)[2], double (&V)[2][2], double (&er_f)[2], double &corCoef)
{
	printf("          ⎛ %- 16.10f ⎞\n", f[0]);
	printf("      f = ⎜                  ⎟\n");
	printf("          ⎝ %- 16.10f ⎠\n\n", f[1]);
	printf("          ⎛ %- 16.10f %- 16.10f ⎞\n", V[0][0], V[0][1]);
	printf("      V = ⎜                                   ⎟\n");
	printf("          ⎝ %- 16.10f %- 16.10f ⎠\n\n", V[1][0], V[1][1]);
	printf("          ⎛ %- 16.10f ⎞\n", er_f[0]);
	printf("f_error = ⎜                  ⎟\n");
	printf("          ⎝ %- 16.10f ⎠\n\n", er_f[1]);
	printf("Correlation Coefficient = %- .10f \n", corCoef);
}

void ex32Work(const char *ex, double eps)
{
	double g[2];
	double f[2];
	double V[2][2];
	double er_f[2];
	double corCoef;

	g[0] = 200;
	g[1] = 169;
	trueEvent<0>(eps, g, f);
	covarianceMEC<0>(eps, g, V, er_f, corCoef);
	printf("%s)\n", ex);
	printf("eps = %.1f \t g_1 = 200 \t g_2 = 169\n\n", eps);
	printResult<0>(f, V, er_f, corCoef);
	printf("\n");
}

void ex32()
{
	ex32Work("d", 0.1);
	ex32Work("e", 0.4);
	ex32Work("f", 0.5);
}

double randomNormalGauss(TRandom *random)
{
	static double cache = 0;
	static bool isCached = false;

	if (isCached)
	{
		isCached = false;
		return cache;
	}
	double v1 = 2 * random->Rndm() - 1;
	double v2 = 2 * random->Rndm() - 1;
	double s = v1 * v1 + v2 * v2;
	if (s >= 1)
	{
		return randomNormalGauss(random);
	}
	double x1 = v1 * sqrt(-2 / s * log(s));
	double x2 = v2 * sqrt(-2 / s * log(s));
	isCached = true;
	cache = x2;
	return x1;
}

double randomGauss(TRandom *random, double mu, double sigma)
{
	return sigma * randomNormalGauss(random) + mu;
}

void randomCorrelatedGauss(TRandom *random, double mu1, double sigma1, double mu2, double sigma2, double correlation, double &r1, double &r2)
{
	double n1 = randomNormalGauss(random);
	double n2 = randomNormalGauss(random);
	r1 = sqrt(1 - correlation * correlation) * sigma1 * n1 + correlation * sigma2 * n2 + mu1;
	r2 = sigma2 * n2 + mu2;
}
void ex34(){
	TRandom *random = new TRandom3();
	static size_t events = 3e4;
	double sigma1 = 3.5;
	double mu1 = 4;
	double sigma2 = 1.5;
	double mu2 = 2;
	double rho = 0.8;

	double xy[2][30000];
	for (size_t i = 0; i < events; i++)
	{
		randomCorrelatedGauss(random, mu1, sigma1, mu2, sigma2, rho, xy[0][i], xy[1][i]);
	}

	TCanvas *canvas = new TCanvas("canvasb");
	canvas->cd();
	TH2D *hist0 = new TH2D("Events", "", 100, -5, 15, 100, -5, 10);
	for (size_t i = 0; i < events; i++)
	{
		hist0->Fill(xy[0][i], xy[1][i]);
	}
	hist0->GetXaxis()->SetTitle("x");
	hist0->GetYaxis()->SetTitle("y");
	hist0->SetMarkerColor(2);
	hist0->SetTitle("2D-Gauss");
	hist0->Draw("COLZ");
	canvas->Update();

#if defined(__MAKE__)
	canvas->SaveAs("34.pdf");
#endif
}

#if defined(__MAKE__)

int main()
{
	ex32();
	ex34();
	return 0;
}

#endif
