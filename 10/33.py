from pylab import *
from scipy.misc import factorial

fact = log(factorial(13) * factorial(9) * factorial(8))

x = linspace(1, 30, 10000)
F = lambda x: -30 * log(x) + 3 * x + fact
G = lambda x: 3 / 20 * (x - 10)**2 + 30 * (1 - log(10)) + fact

from matplotlib.backends.backend_pgf import FigureCanvasPgf
matplotlib.backend_bases.register_backend('pdf', FigureCanvasPgf)
matplotlib.rc("pgf", texsystem="lualatex")
matplotlib.rc("text", usetex=True)
pgf_with_rc_fonts = {
    "font.family": "serif",
    "font.serif": ["Computer Modern Latin"]
}
mpl.rcParams.update(pgf_with_rc_fonts)

axhline(y=F(10), ls="--", c="r")
axhline(y=F(10) + 1 / 2, ls="--", c="r")
axhline(y=F(10) + 2, ls="--", c="r")
axhline(y=F(10) + 9 / 2, ls="--", c="r")
plot(x, G(x), label=r"$G(\mu)$")
plot(x, F(x), label=r"$F(\mu)$")
xlim(1, 30)
xlabel(r"$\mu$")
legend(loc="upper left")
gcf().set_figwidth(8)
savefig("33.pgf")
