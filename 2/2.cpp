#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>

#include "TCanvas.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TGraph.h"

using namespace std;

void ex4()
{
	/*
	 * a
	 */
	ifstream in("Groesse_Gewicht.txt");

	vector<double> size;
	vector<double> weight;

	while(in.good() && !in.eof())
	{
		double s, w;

		in >> s  >> w;
		size.push_back(s);
		weight.push_back(w);
	}
	in.close();

	/*
	 * b
	 */
	TCanvas *canvas1 = new TCanvas("canvas1", "Size, Weight");
	canvas1->Divide(2, 1);

	TH1D *histS = new TH1D("Size", "Size", 40, 50, 190);
	for (auto s : size)
	{
		histS->Fill(s);
	}

	TH1D *histW = new TH1D("Weight", "Weight", 40, 1.6, 2.0);
	for (auto w : weight)
	{
		histW->Fill(w);
	}

	canvas1->cd(1);
	histS->Fit("gaus");
	histS->Draw();
	histS->GetXaxis()->SetTitle("Size");
	canvas1->cd(2);
	histW->Fit("gaus");
	histW->Draw();
	histW->GetXaxis()->SetTitle("Size");
	canvas1->Update();
	
	/*
	 * c
	 */
	TCanvas *canvas2 = new TCanvas("canvas2", "Size, Weight");
	canvas2->Divide(2,1);

	TH2D *hist2 = new TH2D("Size, Weight", "Size, Weight", 40, 50, 190, 40, 1.6, 2.0);
	for (auto s: size)
	{
		for (auto w : weight)
		{
			hist2->Fill(s, w);
		}
	}

	canvas2->cd(1);
	hist2->Draw();
	hist2->GetXaxis()->SetTitle("Size");
	hist2->GetYaxis()->SetTitle("Weight");
	canvas2->cd(2);
	hist2->Draw("COL");
	canvas2->Update();

	/*
	 * d
	 */
	TCanvas *canvas3 = new TCanvas("canvas3", "Size, Weight");
	
	TGraph *graph = new TGraph((int) size.size(), (double *) &size[0], (double *) &weight[0]);
	graph->Sort();
	graph->Fit("pol1");

	canvas3->cd();
	graph->Draw("A*");
	graph->GetXaxis()->SetTitle("Size");
	graph->GetYaxis()->SetTitle("Weight");
	canvas3->Update();

#if !defined(__CINT__)
	canvas1->SaveAs("1D_hist.pdf");
	canvas2->SaveAs("2D_hist.pdf");
	canvas3->SaveAs("xy.pdf");
#endif
}

void ex5()
{
	TGraph *graphA = new TGraph();
	TGraph *graphB = new TGraph();
	TGraph *graphC = new TGraph();
	TGraph *graphD = new TGraph();

	for (volatile float x = 0.9f; x <= 1.1f; x += 0.0001f)
	{
		/*
	 	 * a
	 	 */
		volatile float a = 1.0f;
		for (int i = 0; i < 6; i++)
		{
			a *= 1.0f - x;
		}
		graphA->SetPoint(graphA->GetN(), x, a);

		/*
		 * b
		 */
		volatile float b = x * x * x * x * x * x
		               	   - 6.0f * x * x * x * x * x
		               	   + 15.0f * x * x * x * x
		               	   - 20.0f * x * x * x
		               	   + 15.0f * x * x
		               	   - 6.0f * x
		               	   + 1.0f;
		graphB->SetPoint(graphB->GetN(), x, b);

		/*
		 * c
		 */
		volatile float c  = x - 6.0f;
		               c *= x;
					   c += 15.0f;
		               c *= x;
		               c += -20.0f;
		               c *= x;
		               c += 15.0f;
		               c *= x;
		               c += -6.0f;
		               c *= x;
		               c += 1.0f;
		graphC->SetPoint(graphC->GetN(), x, c);

		/*
		 * d
		 */
		double d  = x - 6.0;
		       d *= x;
			   d += 15.0;
		       d *= x;
		       d += -20.0;
		       d *= x;
		       d += 15.0;
		       d *= x;
		       d += -6.0;
		       d *= x;
		       d += 1.0;
		graphD->SetPoint(graphD->GetN(), x, d);
	}

	/*
	 * e
	 */
	TGraph *graphE = new TGraph();
	for (double x = 0.999; x <= 1.001; x += 0.0000001)
	{
		double e  = x - 6.0;
		       e *= x;
			   e += 15.0;
		       e *= x;
		       e += -20.0;
		       e *= x;
		       e += 15.0;
		       e *= x;
		       e += -6.0;
		       e *= x;
		       e += 1.0;
		graphE->SetPoint(graphE->GetN(), x, e);
	}
	
	TCanvas *canvas = new TCanvas("canvas", "Exercise 5");
	canvas->Divide(2, 2);
	canvas->cd(1);
	graphA->SetTitle("a");
	graphA->Draw("AC");
	canvas->cd(2);
	graphB->SetTitle("b");
	graphB->Draw("AC");
	canvas->cd(3);
	graphC->SetTitle("c");
	graphC->Draw("AC");
	canvas->cd(4);
	graphD->SetTitle("d");
	graphD->Draw("AC");
	canvas->Update();

	TCanvas *canvasE = new TCanvas("canvasE", "Exercise 5e");
	canvasE->cd();
	graphE->Draw("AC");
	canvasE->Update();

#if !defined(__CINT__)
	canvas->SaveAs("5.pdf");
	canvasE->SaveAs("5e.pdf");
#endif
}

void ex6()
{
	cout << "exact: " << -1.0 / 6 << endl;

	for (int i = 1; i <= 20; i++)
	{
		double x = 1.0;
		for (int j = 0; j < i; j++)
		{
			x /= 10;
		}
		cout << "1e-" << i << ": " << (sqrt(9 - x) - 3) / x <<  endl;
	}

	/*
	 * The best results are produced by 1e-4 to 1e-11.
	 * Larger values of x are not near enough to zero to produce the correct result.
	 * For smaller values of x the numerical errors grow and the result worsens.
	 */
}

#if !defined(__CINT__)

int main()
{
	ex4();
	ex5();
	ex6();
	return 0;
}

#endif
