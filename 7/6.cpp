#include <iostream>
#include <cmath>
#include <fstream>

#include "TRandom.h"
#include "TRandom3.h"
#include "TCanvas.h"
#include "TH1D.h"
#include "TH2D.h"

using namespace std;

double randomGauss(TRandom *random, double mu, double sigma)
{
	double v1 = 2 * random->Rndm() - 1;
	double v2 = 2 * random->Rndm() - 1;
	double s = v1 * v1 + v2 * v2;
	if (s >= 1)
	{
		return randomGauss(random, mu, sigma);
	}
	double x1 = v1 * sqrt(-2 / s * log(s));
	return sigma * x1 + mu;
}

void monteCarlo()
{
	int events = 1e5;
	TRandom3 *random = new TRandom3();
	TCanvas *canvas = new TCanvas("canvas", "Detector Simulation");
	canvas->Divide(3, 2);

	/*
	 * 20
	 */
	double *energy = new double[events];
	TH1D *histEnergy = new TH1D("histEnergy", "Monte-Carlo Signal", 100, 1, 10);
	for (int i = 0; i < events; i++)
	{
		energy[i] = pow(random->Rndm(), -1 / 1.7);
		histEnergy->Fill(energy[i]);
	}
	histEnergy->GetXaxis()->SetTitle("E / TeV");
	histEnergy->GetYaxis()->SetTitle("Events");
	canvas->cd(1);
	histEnergy->Draw();

	/*
	 * 21
	 */
	bool *accepted = new bool[events];
	TH1D *histAccepted = new TH1D("histAccepted", "Detector Acceptance", 100, 1, 10);
	for (int i = 0; i < events; i++)
	{
		if (random->Rndm() < pow(1 - exp(-energy[i] / 2), 3))
		{
			accepted[i] = true;
			histAccepted->Fill(energy[i]);
		}
	}
	histAccepted->GetXaxis()->SetTitle("E / TeV");
	histAccepted->GetYaxis()->SetTitle("Events Accepted");
	canvas->cd(2);
	histAccepted->Draw();

	/*
	 * 22
	 */
	int *hits = new int[events];
	TH1D *histHits = new TH1D("histHits", "Hits", 100, 1, 100);
	for (int i = 0; i < events; i++)
	{
		while (hits[i] == 0)
		{
			hits[i] = 10 * randomGauss(random, energy[i], 0.2 * energy[i]);
		}
		histHits->Fill(hits[i]);
	}
	histHits->GetXaxis()->SetTitle("Hits");
	histHits->GetYaxis()->SetTitle("Events");
	canvas->cd(3);
	histHits->Draw();

	/*
	 * 23
	 */
	double *positionX = new double[events];
	double *positionY = new double[events];
	TH2D *histPosition = new TH2D("histPosition", "Position", 100, 4, 10, 100, 0, 6);
	for (int i = 0; i < events; i++)
	{
		positionX[i] = randomGauss(random, 7, 1 / log(hits[i]));
		positionY[i] = randomGauss(random, 3, 1 / log(hits[i]));
		histPosition->Fill(positionX[i], positionY[i]);
	}
	histPosition->GetXaxis()->SetTitle("x");
	histPosition->GetYaxis()->SetTitle("y");
	canvas->cd(4);
	histPosition->Draw("COLZ");

	/*
	 * 24
	 */
	int backEvents = 1e7;
	int *backHits = new int[backEvents];
	TH1D *histBackHits = new TH1D("histBackHits", "Background Hits", 100, 1, 100);
	double *backX = new double[backEvents];
	double *backY = new double[backEvents];
	TH2D *histBackPosition = new TH2D("histBackPosition", "Background Position", 100, 0, 10, 100, 0, 10);
	for (int i = 0; i < backEvents; i++)
	{
		while (backHits[i] == 0)
		{
			backHits[i] = exp(randomGauss(random, 2, 1));
		}
		histBackHits->Fill(backHits[i]);
		double x = randomGauss(random, 0, 1);
		double y = randomGauss(random, 0, 1);
		backX[i] = sqrt(1 - 0.25) * 3 * x + 0.5 * 3 * y + 5;
		backY[i] = 3 * y + 5;
		histBackPosition->Fill(backX[i], backY[i]);
	}
	histBackHits->GetXaxis()->SetTitle("Hits");
	histBackHits->GetYaxis()->SetTitle("Events");
	canvas->cd(5);
	histBackHits->Draw();
	histBackPosition->GetXaxis()->SetTitle("x");
	histBackPosition->GetYaxis()->SetTitle("y");
	canvas->cd(6);
	histBackPosition->Draw("COLZ");

	canvas->Update();

#if defined(__MAKE__)
	canvas->SaveAs("6.pdf");
#endif

	FILE *file = fopen("signal.txt", "w");
	for (int i = 0; i < events; i++)
	{
		fprintf(file, "%.20f %d %d %.20f %.20f\n", energy[i], accepted[i], hits[i], positionX[i], positionY[i]);
	}
	fclose(file);
	file = fopen("background.txt", "w");
	for (int i = 0; i < backEvents; i++)
	{
		fprintf(file, "%d %.20f %.20f\n", backHits[i], backX[i], backY[i]);
	}
	fclose(file);
}

#if defined(__MAKE__)

int main()
{
	monteCarlo();
	return 0;
}

#endif
