#include <fstream>
#include <iostream>
#define _USE_MATH_DEFINES
#include <cmath>

#include "TCanvas.h"
#include "TH1.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TGraph.h"

using namespace std;

template <int hack> // work around bug in ACLiC
void meanCovarianceCorrelation(double *data1, double *data2, size_t length, double (&mean)[2], double (&covariance)[2][2], double &correlation)
{
	/*
	 * Welford's algorithm: https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance#Online_algorithm
	 */
	mean[0] = 0;
	mean[1] = 0;
	double M1 = 0;
	double M2 = 0;
	double C = 0;

	for (size_t i = 0; i < length; i++)
	{
		double delta1 = data1[i] - mean[0];
		double delta2 = data2[i] - mean[1];
		mean[0] += delta1 / (i + 1);
		mean[1] += delta2 / (i + 1);
		M1 += delta1 * (data1[i] - mean[0]);
		M2 += delta2 * (data2[i] - mean[1]);
		C += (data1[i] - mean[0]) * delta2;
	}

	covariance[0][0] = M1 / (length - 1);
	covariance[1][1] = M2 / (length - 1);
	covariance[0][1] = covariance[1][0] = C / length;
	correlation = C / (sqrt(M1) * sqrt(M2));
}

template <size_t length>
void vectorSubtract(double (&v1)[length], double (&v2)[length], double (&r)[length])
{
	for (size_t i = 0; i < length; i++)
	{
		r[i] = v1[i] - v2[i];
	}
}

template <size_t rows, size_t cols>
void matrixAdd(double (&m1)[rows][cols], double (&m2)[rows][cols], double (&r)[rows][cols])
{
	for (size_t i = 0; i < rows; i++)
	{
		for (size_t j = 0; j < cols; j++)
		{
			r[i][j] = m1[i][j] + m2[i][j];
		}
	}
}

template <int hack>
void matrix2Inverse(double (&m)[2][2], double (&r)[2][2])
{
	double det = m[0][0] * m[1][1] - m[1][0] * m[0][1];
	r[0][0] = m[1][1] / det;
	r[0][1] = -m[0][1];
	r[1][0] = -m[1][0];
	r[1][1] = m[0][0] / det;
}

template <size_t rows, size_t cols>
void matrixMultiply(double (&m)[rows][cols], double (&v)[cols], double (&r)[rows])
{
	for (size_t i = 0; i < rows; i++)
	{
		r[i] = 0;
		for (size_t j = 0; j < cols; j++)
		{
			r[i] += m[i][j] * v[j];
		}
	}
}

template <size_t length>
double vectorMultiply(double (&v1)[length], double (&v2)[length])
{
	double sum = 0;
	for (size_t i = 0; i < length; i++)
	{
		sum += v1[i] * v2[i];
	}
	return sum;
}

template <int hack>
void fisherDiscriminant(double *dataX, double *dataY, size_t length, double (&a)[2], double *lambda)
{
	for (size_t i = 0; i < length; i++)
	{
		double d[2] = {dataX[i], dataY[i]};
		lambda[i] = vectorMultiply<2>(a, d);
	}
}

int getMaximumIndex(double *data, size_t length)
{
	double max = data[0];
	int index = 0;
	for (size_t i = 1; i < length; i++)
	{
		if (data[i] > max)
		{
			max = data[i];
			index = i;
		}
	}
	return index;
}

void discrimantAnalysis()
{
	/*
	 * 25 b
	 */
	size_t events = 1e5;
	double *energy = new double[events];
	bool *accepted = new bool[events];
	int *hits = new int[events];
	double *positionX = new double[events];
	double *positionY = new double[events];

	FILE *file = fopen("signal.txt", "r");
	for (size_t i = 0; i < events; i++)
	{
		int tmp;
		fscanf(file, " %lf %d %d %lf %lf", &energy[i], &tmp, &hits[i], &positionX[i], &positionY[i]);
		accepted[i] = tmp;
	}
	fclose(file);

	size_t backEvents = 1e7;
	int *backHits = new int[backEvents];
	double *backX = new double[backEvents];
	double *backY = new double[backEvents];

	file = fopen("background.txt", "r");
	for (size_t i = 0; i < backEvents; i++)
	{
		fscanf(file, " %d %lf %lf", &backHits[i], &backX[i], &backY[i]);
	}
	fclose(file);

	TCanvas *canvas = new TCanvas("canvas25");
	canvas->cd();
	TH2D *positionHist = new TH2D("positionHist", "Position", 100, 0, 10, 100, 0, 10);
	for (size_t i = 0; i < events; i++)
	{
		positionHist->Fill(positionX[i], positionY[i]);
	}
	for (size_t i = 0; i < backEvents; i++)
	{
		positionHist->Fill(backX[i], backY[i]);
	}
	positionHist->GetXaxis()->SetTitle("x");
	positionHist->GetYaxis()->SetTitle("y");
	positionHist->Draw("COLZ");

	canvas->Update();

#if defined(__MAKE__)
	canvas->SaveAs("25.pdf");
#endif

	/*
	 * 25 c
	 * 26 a
	 */
	double meanSignal[2];
	double covarianceSignal[2][2];
	double correlationSignal;
	double meanBack[2];
	double covarianceBack[2][2];
	double correlationBack;
	double mean[2];
	double covariance[2][2];
	double correlation;

	meanCovarianceCorrelation<0>(positionX, positionY, events, meanSignal, covarianceSignal, correlationSignal);
	meanCovarianceCorrelation<0>(backX, backY, backEvents, meanBack, covarianceBack, correlationBack);

	mean[0] = (events * meanSignal[0] + backEvents * meanBack[0]) / (events + backEvents);
	mean[1] = (events * meanSignal[1] + backEvents * meanBack[1]) / (events + backEvents);
	covariance[0][0] = (covarianceSignal[0][0] * (events - 1) + covarianceBack[0][0] * (backEvents - 1) + (meanSignal[0] - meanBack[0]) * (meanSignal[0] - meanBack[0]) * events * backEvents / (events + backEvents)) / (events + backEvents - 1);
	covariance[1][1] = (covarianceSignal[1][1] * (events - 1) + covarianceBack[1][1] * (backEvents - 1) + (meanSignal[1] - meanBack[1]) * (meanSignal[1] - meanBack[1]) * events * backEvents / (events + backEvents)) / (events + backEvents - 1);
	covariance[0][1] = covariance[1][0] = (covarianceSignal[0][1] * events + covarianceBack[0][1] * backEvents + (meanSignal[0] - meanBack[0]) * (meanSignal[1] - meanBack[1]) * events * backEvents / (events + backEvents)) / (events + backEvents);
	correlation = covariance[0][1] * (events + backEvents) / (sqrt(covariance[0][0] * (events + backEvents - 1)) * sqrt(covariance[1][1] * (events + backEvents - 1)));

	printf("Signal: Mean:                    ( %- .10f %- .10f )\n", meanSignal[0], meanSignal[1]);
	printf("        Covariance-Matrix:       ⎛ %- .10f %- .10f ⎞\n", covarianceSignal[0][0], covarianceSignal[0][1]);
	printf("                                 ⎜                             ⎟\n");
	printf("                                 ⎝ %- .10f %- .10f ⎠\n", covarianceSignal[1][0], covarianceSignal[1][1]);
	printf("        Correlation-Coefficient:   %- .10f\n", correlationSignal);
	printf("Back:   Mean:                    ( %- .10f %- .10f )\n", meanBack[0], meanBack[1]);
	printf("        Covariance-Matrix:       ⎛ %- .10f %- .10f ⎞\n", covarianceBack[0][0], covarianceBack[0][1]);
	printf("                                 ⎜                             ⎟\n");
	printf("                                 ⎝ %- .10f %- .10f ⎠\n", covarianceBack[1][0], covarianceBack[1][1]);
	printf("        Correlation-Coefficient:   %- .10f\n", correlationBack);
	printf("Total:  Mean:                    ( %- .10f %- .10f )\n", mean[0], mean[1]);
	printf("        Covariance-Matrix:       ⎛ %- .10f %- .10f ⎞\n", covariance[0][0], covariance[0][1]);
	printf("                                 ⎜                             ⎟\n");
	printf("                                 ⎝ %- .10f %- .10f ⎠\n", covariance[1][0], covariance[1][1]);
	printf("        Correlation-Coefficient:   %- .10f\n", correlation);

	/*
	 * 26 b
	 */
	double W[2][2];
	double Winverse[2][2];
	double a[2];
	double b[2];

	vectorSubtract<2>(meanSignal, meanBack, b);
	matrixAdd<2, 2>(covarianceSignal, covarianceBack, W);
	matrix2Inverse<0>(W, Winverse);
	matrixMultiply<2, 2>(Winverse, b, a);

	double *lambdaSignal = new double[events];
	double *lambdaBack = new double[backEvents];
	fisherDiscriminant<0>(positionX, positionY, events, a, lambdaSignal);
	fisherDiscriminant<0>(backX, backY, backEvents, a, lambdaBack);

	/*
	 * 26 c
	 */
	size_t bins = 100;
	double minBin = -100;
	double maxBin = 100;
	canvas = new TCanvas("canvas26c");
	canvas->Divide(1, 3);

	TH1D *histLambdaSignal = new TH1D("histLambdaSignal", "lambda Signal", bins, minBin, maxBin);
	histLambdaSignal->FillN(events, lambdaSignal, NULL);
	canvas->cd(1);
	histLambdaSignal->Draw();

	TH1D *histLambdaBack = new TH1D("histLambdaBack", "lambda Back", bins, minBin, maxBin);
	histLambdaBack->FillN(backEvents, lambdaBack, NULL);
	canvas->cd(2);
	histLambdaBack->Draw();

	TH1D *histLambda = new TH1D("histLambda", "lambda", bins, minBin, maxBin);
	histLambda->Add(histLambdaSignal, histLambdaBack);
	canvas->cd(3);
	histLambda->Draw();

	canvas->Update();

#if defined(__MAKE__)
	canvas->SaveAs("26c.pdf");
#endif

	/*
	 * 26 d
	 */
	double *lambdaX = new double[bins];
	double *cumulLambdaSignal = new double[bins];
	double *cumulLambdaBack = new double[bins];
	double *cumulLambda = new double[bins];

	lambdaX[0] = minBin + (minBin + maxBin) / 2;
	cumulLambdaSignal[0] = histLambdaSignal->GetBinContent(1);
	cumulLambdaBack[0] = histLambdaBack->GetBinContent(1);
	cumulLambda[0] = histLambda->GetBinContent(1);
	for (size_t i = 1; i < bins; i++)
	{
		lambdaX[i] = minBin + i * (maxBin - minBin) / bins + (minBin + maxBin) / 2;
		cumulLambdaSignal[i] = cumulLambdaSignal[i - 1] + histLambdaSignal->GetBinContent(i + 1);
		cumulLambdaBack[i] = cumulLambdaBack[i - 1] + histLambdaBack->GetBinContent(i + 1);
		cumulLambda[i] = cumulLambda[i - 1] + histLambda->GetBinContent(i + 1);
	}

	double *puritySignal = new double[bins];
	double *purityBack = new double[bins];
	double *efficiencySignal = new double[bins];
	double *efficiencyBack = new double[bins];

	for (size_t i = 0; i < bins; i++)
	{
		puritySignal[i] = cumulLambdaSignal[i] / cumulLambda[i];
		purityBack[i] = (backEvents - cumulLambdaBack[i]) / (events + backEvents - cumulLambda[i]);
		efficiencySignal[i] = cumulLambdaSignal[i] / events;
		efficiencyBack[i] = cumulLambdaBack[i] / backEvents;
	}

	canvas = new TCanvas("canvas26d");
	canvas->Divide(2,2);

	TGraph *graphPuritySignal = new TGraph(bins, lambdaX, puritySignal);
	graphPuritySignal->GetXaxis()->SetTitle("Lambda");
	graphPuritySignal->GetYaxis()->SetTitle("Purity");
	graphPuritySignal->SetTitle("Purity Signal");
	canvas->cd(1);
	graphPuritySignal->Draw("AL");

	TGraph *graphPurityBack = new TGraph(bins, lambdaX, purityBack);
	graphPurityBack->GetXaxis()->SetTitle("Lambda");
	graphPurityBack->GetYaxis()->SetTitle("Purity");
	graphPurityBack->SetTitle("Purity Back");
	canvas->cd(2);
	graphPurityBack->Draw("AL");

	TGraph *graphEfficiencySignal = new TGraph(bins, lambdaX, efficiencySignal);
	graphEfficiencySignal->GetXaxis()->SetTitle("Lambda");
	graphEfficiencySignal->GetYaxis()->SetTitle("Efficiency");
	graphEfficiencySignal->SetTitle("Efficiency Signal");
	canvas->cd(3);
	graphEfficiencySignal->Draw("AL");

	TGraph *graphEfficiencyBack = new TGraph(bins, lambdaX, efficiencyBack);
	graphEfficiencyBack->GetXaxis()->SetTitle("Lambda");
	graphEfficiencyBack->GetYaxis()->SetTitle("Efficiency");
	graphEfficiencyBack->SetTitle("Efficiency Back");
	canvas->cd(4);
	graphEfficiencyBack->Draw("AL");

	canvas->Update();

#if defined(__MAKE__)
	canvas->SaveAs("26d.pdf");
#endif

	/*
	 * 26 e
	 */
	double *signalBackRatio = new double[bins];

	for (size_t i = 0; i < bins; i++)
	{
		signalBackRatio[i] = puritySignal[i] / purityBack[i];
	}

	canvas = new TCanvas("canvas26e");

	TGraph *graphRatioEfficiency = new TGraph(bins, lambdaX, signalBackRatio);
	graphRatioEfficiency->GetXaxis()->SetTitle("Lambda");
	graphRatioEfficiency->GetYaxis()->SetTitle("S / B");
	graphRatioEfficiency->SetTitle("Signal-Background-Ratio");
	canvas->cd();
	graphRatioEfficiency->Draw("AL");

	canvas->Update();

	cout << "Signal-Background Ratio: Maximum for Purity at lambda = " << lambdaX[getMaximumIndex(signalBackRatio, bins)] << endl;

#if defined(__MAKE__)
	canvas->SaveAs("26e.pdf");
#endif

	/*
	 * 26 f
	 */
	double *significance = new double[bins];

	for (size_t i = 0; i < bins; i++)
	{
		significance[i] = puritySignal[i] / sqrt(purityBack[i] + puritySignal[i]);
	}

	canvas = new TCanvas("canvas26f");

	TGraph *graphSignificanceEfficiency = new TGraph(bins, lambdaX, significance);
	graphSignificanceEfficiency->GetXaxis()->SetTitle("Lambda");
	graphSignificanceEfficiency->GetYaxis()->SetTitle("Significance");
	graphSignificanceEfficiency->SetTitle("Significance");
	canvas->cd();
	graphSignificanceEfficiency->Draw("AL");

	canvas->Update();

	cout << "Significance: Maximum for Purity at lambda = " << lambdaX[getMaximumIndex(significance, bins)] << endl;

#if defined(__MAKE__)
	canvas->SaveAs("26f.pdf");
#endif
}

#if defined(__MAKE__)

int main()
{
	discrimantAnalysis();
	return 0;
}

#endif
